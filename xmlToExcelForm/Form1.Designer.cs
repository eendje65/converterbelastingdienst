﻿namespace xmlToExcelForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sourceSelectBtn = new System.Windows.Forms.Button();
            this.targetPathSelectBtn = new System.Windows.Forms.Button();
            this.AddXmlBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // sourceSelectBtn
            // 
            this.sourceSelectBtn.Location = new System.Drawing.Point(83, 29);
            this.sourceSelectBtn.Name = "sourceSelectBtn";
            this.sourceSelectBtn.Size = new System.Drawing.Size(108, 23);
            this.sourceSelectBtn.TabIndex = 0;
            this.sourceSelectBtn.Text = "Select Source";
            this.sourceSelectBtn.UseVisualStyleBackColor = true;
            this.sourceSelectBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // targetPathSelectBtn
            // 
            this.targetPathSelectBtn.Location = new System.Drawing.Point(83, 69);
            this.targetPathSelectBtn.Name = "targetPathSelectBtn";
            this.targetPathSelectBtn.Size = new System.Drawing.Size(108, 23);
            this.targetPathSelectBtn.TabIndex = 1;
            this.targetPathSelectBtn.Text = "Select Target Path";
            this.targetPathSelectBtn.UseVisualStyleBackColor = true;
            this.targetPathSelectBtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // AddXmlBtn
            // 
            this.AddXmlBtn.Enabled = false;
            this.AddXmlBtn.Location = new System.Drawing.Point(83, 110);
            this.AddXmlBtn.Name = "AddXmlBtn";
            this.AddXmlBtn.Size = new System.Drawing.Size(108, 23);
            this.AddXmlBtn.TabIndex = 2;
            this.AddXmlBtn.Text = "Add xml";
            this.AddXmlBtn.UseVisualStyleBackColor = true;
            this.AddXmlBtn.Click += new System.EventHandler(this.AddXmlBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 168);
            this.Controls.Add(this.AddXmlBtn);
            this.Controls.Add(this.targetPathSelectBtn);
            this.Controls.Add(this.sourceSelectBtn);
            this.Name = "Form1";
            this.Text = "Convert Xml to Excel";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button sourceSelectBtn;
        private System.Windows.Forms.Button targetPathSelectBtn;
        private System.Windows.Forms.Button AddXmlBtn;
    }
}


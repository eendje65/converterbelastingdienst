﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Excel = Microsoft.Office.Interop.Excel;
namespace xmlToExcelForm
{
    public partial class Form1 : Form
    {
        private static int _primaryColumn = 6;
        private readonly OpenFileDialog _openFileDialog = new OpenFileDialog();
        private readonly FolderBrowserDialog _folderBrowserDialog = new FolderBrowserDialog();
        private string _sourcePath;
        private string _targetPath;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var result = _folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                _sourcePath = _folderBrowserDialog.SelectedPath;
            }
            CheckSetPathsSet();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var result = _openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                var extension = GetExtension(_openFileDialog.FileName);
                if (extension == ".xlsx")
                {
                    _targetPath = _openFileDialog.FileName;
                }
                else
                {
                    _targetPath = null;
                    MessageBox.Show("Target does not have a .xlsx extension");
                }

            }
            CheckSetPathsSet();
        }

        private string GetExtension(string fileName)
        {
            return fileName.Substring(fileName.LastIndexOf("."));
        }

        private void AddXmlBtn_Click(object sender, EventArgs e)
        {
            string[] files;
            try
            {
                files = Directory.GetFiles(_sourcePath);
            }
            catch (System.IO.DirectoryNotFoundException ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            List<string> values = new List<string>();
            List<string> texts = new List<string>();
            //read xml
            for (int i = 0; i < files.Length; i++)
            {
                var file = files[i];
                if (GetExtension(file) == ".xml")
                {
                    using (XmlReader reader = XmlReader.Create(file))
                    {
                        while (reader.Read())
                        {
                            if (reader.IsStartElement())
                            {
                                var text = reader.GetAttribute("Text");
                                var value = reader.GetAttribute("Value");
                                if (i == 0)
                                {
                                    texts.Add(text);
                                }
                                values.Add(value);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show($"Extension of file {file} is not .xml {GetExtension(file)}");
                }
            }

            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Sheets xlWorkSheets;
            Excel.Workbooks xlWorkbooks;

            try
            {
                xlWorkbooks = xlApp.Workbooks;
                xlWorkBook = xlWorkbooks.Open(_targetPath);
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            xlWorkSheets = xlWorkBook.Worksheets;
            xlWorkSheet = (Excel.Worksheet) xlWorkSheets.Item["Blad1"];

            var nrOfCols = texts.Count;
            var nrOfRowsUsed = 0;

            List<string> existingKeys = new List<string>();
            // get keys from worksheet to check for double
            for (var i = 1; i <= int.MaxValue; i++)
            {
                var key = xlWorkSheet.Cells[i, _primaryColumn] as Excel.Range;
                if (key != null)
                {
                    if (key.Value2 == null)
                    {
                        nrOfRowsUsed = i - 2; // 2, omdat je 1 te ver zit en van 1 naar 0-based gaat
                        break;
                    }
                    string keyData = key.Value2.ToString();
                    existingKeys.Add(keyData);
                }
            }

            int previousNrOfRowsUsed = nrOfRowsUsed;

            var data = xlWorkSheet.Cells[1, 3];

            var range = xlWorkSheet.Cells[1, 3] as Excel.Range;
            if (range != null)
                data = (string)range.Value2;

            //add  headers
            if (data == null)
            {
                for (int i = 0; i < texts.Count; i++)
                {
                    xlWorkSheet.Cells[1, i + 1] = texts[i];
                }
                nrOfRowsUsed++;
            }

            nrOfRowsUsed++;
            // add values to worksheet
            for (int i = 0; i < values.Count; i++)
            {
                if (i % nrOfCols == 0)
                {
                    nrOfRowsUsed++;
                }

                int x = (i % nrOfCols) + 1;

                int index = i / nrOfCols;
                index *= nrOfCols; // niet het zelfde vanwege integer deling
                index += (_primaryColumn - 1) % nrOfCols;

                string key = values[index];
                if (!existingKeys.Contains(key))
                {
                    xlWorkSheet.Cells[nrOfRowsUsed, x] = values[i];
                }
            }

            try
            {
                xlWorkBook.Save();
                xlWorkBook.Close(false);
                xlApp.Quit();
                MessageBox.Show($"Done adding xml to excel");
                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkSheets);
                Marshal.ReleaseComObject(xlWorkBook); 
                Marshal.ReleaseComObject(xlWorkbooks);

                Marshal.ReleaseComObject(xlApp);

            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CheckSetPathsSet()
        {
            if (_sourcePath != null && _targetPath != null)
            {
                AddXmlBtn.Enabled = true;
            }
        }
    }
}